import xml.dom.minidom

domArticle = xml.dom.minidom.parse("data-metabolicNetworkReconstruction/17132165.xml")

CommentsCorrectionsListElts = domArticle.getElementsByTagName("CommentsCorrectionsList")
for currentCommentsElt in CommentsCorrectionsListElts:
    CommentsValue = ""
    CommentsListNameList = currentCommentsElt.getElementsByTagName("CommentsCorrections")
    PMIDElt = CommentsListNameList.getElementsByTagName("PMID")
    FirstPMIDElt = PMIDElt[0]
    PMIDValue = FirstPMIDElt.childNodes[0].data
    print(PMIDValue)

