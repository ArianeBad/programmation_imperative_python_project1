import xml.dom.minidom

domArticle = xml.dom.minidom.parse("data-metabolicNetworkReconstruction/12801875.xml")

authorElts = domArticle.getElementsByTagName("authors")
for currentAuthorElt in authorElts:
    authorLastNameValue = ""
    authorFirstNameValue = ""
# we assume that both elements
# - are present
# - are unique
#so getElementsByTagName(...) returns a list of 1 element
    authorLastNameList = currentAuthorElt.getElementsByTagName("LastName")
    authorFirstNameList = currentAuthorElt.getElementsByTagName("ForeName")
    if (authorLastNameList.length==1) and (authorFirstNameList.length==1):
        authorLastNameElt = authorLastNameList[0]
        authorFirstNameElt = authorFirstNameList[0]
# we assume that both elements only contain text
# (so they only have 1 child)
        authorLastNameValue = authorLastNameElt.childNodes[0].data
        authorFirstNameValue = authorFirstNameElt.childNodes[0].data
    print(authorLastNameValue + "\t" + authorFirstNameValue)
